<?php

namespace WebalizeMeeting;

class FamilyTree implements IComponent
{
    private array $children = [];

    public function addPerson(IComponent $person): void
    {
        $this->children[] = $person;
    }

    public function getAgeSum(): int
    {
        $sum = 0;

        foreach ($this->children as $child) {
            $sum += $child->getAgeSum();
        }

        return $sum;
    }
}