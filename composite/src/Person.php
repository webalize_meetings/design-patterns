<?php

namespace WebalizeMeeting;

class Person implements IComponent
{
    private array $children = [];
    private int $age;

    public function __construct(int $age)
    {
        $this->age = $age;
    }

    public function addPerson(IComponent $person): void
    {
        $this->children[] = $person;
    }

    public function getAgeSum(): int
    {
        $sum = $this->age;

        foreach ($this->children as $child) {
            $sum += $child->getAgeSum();
        }

        return $sum;
    }
}