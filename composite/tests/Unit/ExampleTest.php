<?php

namespace WebalizeTest\Unit;

use WebalizeMeeting\FamilyTree;
use WebalizeMeeting\Person;
use WebalizeTest\TestCase;

class ExampleTest extends TestCase
{
    public function test_if_age_sum_is_correct(): void
    {
        $grandFatherF = new Person(70);
        $grandMotherF = new Person(70);
        $grandFatherM = new Person(70);
        $grandMotherM = new Person(70);
        $father = new Person(30);
        $mother = new Person(30);
        $child = new Person(5);

        $child->addPerson($mother);
        $child->addPerson($father);
        $father->addPerson($grandFatherF);
        $father->addPerson($grandMotherF);
        $mother->addPerson($grandFatherM);
        $mother->addPerson($grandMotherM);

        $familyTree = new FamilyTree();
        $familyTree->addPerson($child);

        $this->assertEquals(345, $familyTree->getAgeSum());
    }
}