<?php

namespace WebalizeMeeting\Observer;

use WebalizeMeeting\Observable\IObservable;

class ResearchStation implements IObserver
{

    private int $stationNo;

    public function __construct(IObservable $observable, int $stationNo)
    {
        $observable->registerObserver($this);
        $this->stationNo = $stationNo;
    }

    public function update($state): void
    {
        dump("updated station: {$this->stationNo} new tmp=$state");
    }
}