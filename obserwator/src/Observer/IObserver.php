<?php

namespace WebalizeMeeting\Observer;

use WebalizeMeeting\Observable\IObservable;

interface IObserver
{
    public function __construct(IObservable $observable, int $stationNo);

    public function update($state);
}