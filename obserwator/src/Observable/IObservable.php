<?php

namespace WebalizeMeeting\Observable;

use WebalizeMeeting\Observer\IObserver;

interface IObservable
{
    public function registerObserver(IObserver $observer);

    public function deregisterObserver(IObserver $observer);

    public function notify();

    public function setState(float $state);
}