<?php

namespace WebalizeTest\Unit;

use PhpParser\Comment;
use WebalizeMeeting\AdamDrink;
use WebalizeMeeting\Pipeline\CoffeeService;
use WebalizeMeeting\Pipeline\Nodes\EspressoShot;
use WebalizeMeeting\Pipeline\Nodes\Milk;
use WebalizeMeeting\Pipeline\Nodes\Sugar;
use WebalizeTest\TestCase;

class PipelineTest extends TestCase
{
    public function test_node(): void
    {
        $milk = new Milk(); //100
        $sugar = new Sugar(); //20
        $espresso = new EspressoShot(); //200

        $milk->setNextNode($sugar);
        $sugar->setNextNode($espresso);


        $this->assertEquals(570, $milk->getCost(250));
    }

    public function test_pipeline_service(): void
    {
        $coffeeService = new CoffeeService();

        $coffeeService->setAddons([
            new Milk(),
            new Sugar(),
            new EspressoShot(),
        ]);

        $this->assertEquals(570, $coffeeService->getCost(250));
    }

    public function test_pipeline_service_works_without_addons(): void
    {
        $coffeeService = new CoffeeService();

        $this->assertEquals(250, $coffeeService->getCost(250));
    }
}