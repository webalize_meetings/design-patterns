<?php

namespace WebalizeMeeting\Pipeline;

interface IPipeline
{
    public function setNextNode(IPipeline $next);
    public function getCost($cost);
}