<?php

namespace WebalizeMeeting\Pipeline\Nodes;

use WebalizeMeeting\Pipeline\IPipeline;

class Milk implements IPipeline
{
    private IPipeline $next;
    public const PRICE = 100;

    public function getCost($cost): int
    {
        dump('Milk');
        if(!isset($this->next)){
            return $cost + self::PRICE;
        }

        return $this->next->getCost($cost + self::PRICE);
    }

    public function setNextNode(IPipeline $next): void
    {
        $this->next = $next;
    }
}
