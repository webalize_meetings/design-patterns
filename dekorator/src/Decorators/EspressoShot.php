<?php

namespace WebalizeMeeting\Decorators;

use WebalizeMeeting\Drinks\IDrink;

class EspressoShot implements IDecorator
{
    private IDrink $drink;

    /**
     * @param IDrink $drink
     */
    public function __construct(IDrink $drink)
    {
        $this->drink = $drink;
    }

    public function getPrice(): int
    {
        dump('espresso');
        $price = $this->drink->getPrice();
        dump('espresso');
        return $price + 50;
    }
}