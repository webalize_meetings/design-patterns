<?php

namespace WebalizeMeeting\Decorators;

use WebalizeMeeting\Drinks\IDrink;

class Sugar implements IDecorator
{
    private IDrink $drink;

    /**
     * @param IDrink $drink
     */
    public function __construct(IDrink $drink)
    {
        $this->drink = $drink;
    }

    public function getPrice(): int
    {
        dump('sugar');
        $price = $this->drink->getPrice();
        dump('sugar');
        return $price + 50;
    }
}