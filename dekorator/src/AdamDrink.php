<?php

namespace WebalizeMeeting;

use WebalizeMeeting\Decorators\EspressoShot;
use WebalizeMeeting\Decorators\Milk;
use WebalizeMeeting\Decorators\Sugar;
use WebalizeMeeting\Drinks\Coffee;
use WebalizeMeeting\Drinks\IDrink;

class AdamDrink
{
    public function getDrink(): IDrink
    {
        return new Sugar(
            new Milk(
                new EspressoShot(
                    new EspressoShot(
                        new Coffee()
                    )
                )
            )
        );
    }
}