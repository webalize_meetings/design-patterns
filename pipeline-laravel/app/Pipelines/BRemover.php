<?php

namespace App\Pipelines;

class BRemover implements IPipeline
{
    public function handle($variable, \Closure $next)
    {
        $variable = str_replace(['b', 'B'], '', $variable);

        return $next($variable);
    }
}
