<?php

namespace App\Pipelines;

class ARemover implements IPipeline
{
    public function handle($variable, \Closure $next)
    {
        $variable = str_replace(['a', 'A'], '', $variable);

        return $next($variable);
    }
}
