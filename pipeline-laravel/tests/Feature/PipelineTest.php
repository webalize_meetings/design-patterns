<?php

namespace Tests\Feature;

use App\Pipelines\ARemover;
use App\Pipelines\BRemover;
use Illuminate\Pipeline\Pipeline;
use Tests\TestCase;

class PipelineTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_pipeline()
    {
        $pipelineService = new Pipeline();

        $variable = $pipelineService
            ->send('aaaabbcccdddeee')
            ->through([
                new ARemover(),
                new BRemover(),
            ])
            ->thenReturn();

        $this->assertEquals('cccdddeee',$variable);
    }
}
